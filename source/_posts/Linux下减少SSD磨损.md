---
title: Linux下减少SSD磨损
date: 2019-01-28 00:16:52
tags: Linux折腾手册
---
SSD容量越小，在相同写入量的情况下，单颗闪存遭到磨损的次数就越多。而闪存寿命由磨损次数决定，所以SSD容量越小寿命越短。想想我那可怜的120GTLC颗粒SSD可能经不起我多久的暴力使用就可能寿终正寝了。所以，今天就要给他减负，将操作系统系统下的缓存、部分日志丢给内存保管。
<!-- more -->
## 修改/etc/fstab

目的是将缓存、日志等高频率写入操作都写于内存中，避免对SSD的高频率写入操作。

    # vim /etc/fstab

    tmpfs /tmp tmpfs defaults,noatime,nodiratime,mode=1777 0 0

    tmpfs /var/log tmpfs defaults,noatime,mode=1777 0 0

    tmpfs /var/tmp tmpfs defaults,noatime,mode=1777 0 0

### 注释掉swapfile一行

    # /swapfile none swap defaults 0 0

## 重启系统

    # reboot

> 2018-11-14更新：SSD终究还是挂了，还是要老老实实的买个大点的SSD才行。 （
