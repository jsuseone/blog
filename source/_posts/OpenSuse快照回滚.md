---
title: OpenSuse快照回滚
date: 2019-03-28 00:11:57
tags: Linux折腾手册
---

## 起因

手贱降级关键的系统软件包,导致基于GTK2的所有软件都无法启动,试图升级libgtk2.0无果.无奈之下使用OpenSuse自带快照管理工具Snapper进行抢救.
<!-- more -->
## 读取快照

重启进入Grub界面,选择

    Start bootloader from a readonly snapshot

然后选一个能用的快照启动.

## 快照回滚

进入系统后执行:

    sudo snapper rollback 快照ID

其中快照ID可以在Yast里的“杂项”-->“Snapper”找到.

## 最后重启一下系统

这样,大蜥蜴就~~又能再次~~活蹦乱跳起来了.
