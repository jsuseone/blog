---
title: Typecho迁移到Hexo
date: 2019-03-28 00:18:14
tags: 博客迁移
categories: 教程
---

今天总算是把Typecho博客迁移到Hexo了。毕竟Hexo是静态博客，也不用折腾太多东西，就有不错的性能和安全性，这一点很吸引我。这里记录下折腾心得，要详细教程的请移步[官方文档](https://hexo.io/docs)。
<!-- more -->
## 安装Hexo框架

在服务器上将Nodejs装好后，就可以开始安装了。 其实也就4条命令就搭建起来了，非常方便。

    npm install hexo-cli -g
    hexo init blog
    cd blog
    npm install

### 安装Caddy个人版

    # CADDY_TELEMETRY=on curl https://getcaddy.com | bash -s personal

配置web根目录到public，以及写好Systemd service，这里假设你的服务器Caddy配置文件在/etc/中，并且命名为caddy.conf

    # vim /etc/caddy.conf

    you domain
    {
      log /path/to/hexo/caddy.log
        gzip
        root /path/to/hexo/public
    }

#### 注意

上述配置文件中需要按照你的服务器信息进行修改

*   you domain 改为 你服务器的域名
*   /path/to/hexo/ 改为 你服务器中Hexo的路径

### 创建Systemd service

    # vim /etc/systemd/system/caddy.service

    [Unit]
    Description=Caddy HTTP/2 web server
    After=syslog.target
    After=network.target

    [Service]
    User=web
    Group=web
    LimitNOFILE=64000
    ExecStart=/usr/local/bin/caddy --conf=/etc/caddy.conf
    Restart=on-failure

    [Install]
    WantedBy=multi-user.target

### 激活Systemd service

    # systemctl enable caddy.service

### 重启系统

    # reboot

## 安装主题

我这里用的是[melody](https://molunerfinn.com/hexo-theme-melody-doc/#features).

## 评论系统

Hexo是一个静态博客框架,须使用第三方评论系统。这里为了偷懒，用的是在主题中已经配置好布局的DisqusJS，配置方法在项目[主页上有](https://github.com/SukkaW/DisqusJS)。

## 导出文章

文章导出的过程不算麻烦，github上面有php写的[工具](https://github.com/NewbMiao/typecho2Hexo)可以直接将typecho数据库里的文章导出成.md文件，就是文章的发布时间不会被保留这点比较麻烦。而且typecho和hexo的markdown格式也有不同，hexo的标题前的#号后方必须有空格。不然

##就会变成这样子

## 发布文章

Hexo还没有内置像Typecho的编辑器那样的东西，需要自己另外用编辑器编辑好后再上传到`sorce/_post/`下才会发布，相比于Typecho的一键发布，明显繁琐不少。

## 编辑器

我用的是vim，它自带markdown语法高亮。当然，很多文本编辑器都具备这个特性，就连gedit也不例外。

### 插入图片

如果觉得MarkDown插入图片过于繁琐，可以考虑使用~~我完全没有用过的~~[PicGo](https://github.com/Molunerfinn/PicGo)，这个工具可以大大减少插入图片的繁琐步骤，在上传图片之后自动会将图片链接复制到你的剪贴板里。
