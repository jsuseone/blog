---
title: Zsh自动ls
date: 2019-03-28 00:13:59
tags: Linux折腾手册
---
只需要将以下代码加 zshrc 中即可。 `function chpwd() { emulate -L zsh ls -a } `
其实就是使用了function chpwd函数并在函数中执行ls -a而已，关于这个函数，[官方文档](http://zsh.sourceforge.net/Doc/Release/Functions.html#Hook-Functions)中有详细描述。
