---
title: 安利：大学霸 Kail Linux 安全渗透教程
date: 2019-04-05 22:24:52
tags: 渗透教程收藏
---
~~猜猜我捡到了啥？~~
<!--more-->
这篇教程囊括了以下内容：
- [Linux安全渗透简介](https://wizardforcel.gitbooks.io/daxueba-kali-linux-tutorial/content/0.html)
- [配置Kali Linux](https://wizardforcel.gitbooks.io/daxueba-kali-linux-tutorial/content/7.html)
- [高级测试实验室](https://wizardforcel.gitbooks.io/daxueba-kali-linux-tutorial/content/13.html)
- [信息收集](https://wizardforcel.gitbooks.io/daxueba-kali-linux-tutorial/content/16.html)
- [漏洞扫描](https://wizardforcel.gitbooks.io/daxueba-kali-linux-tutorial/content/26.html)
- [漏洞利用](https://wizardforcel.gitbooks.io/daxueba-kali-linux-tutorial/content/29.html)
- [权限提升](https://wizardforcel.gitbooks.io/daxueba-kali-linux-tutorial/content/35.html)
- [密码攻击](https://wizardforcel.gitbooks.io/daxueba-kali-linux-tutorial/content/40.html)
- [无线网络渗透测试](https://wizardforcel.gitbooks.io/daxueba-kali-linux-tutorial/content/40.html)

真是捡到宝了.

![huaji](https://github.com/jsuseone/jsuseone.github.io/blob/master/images/huajidancing.gif?raw=true)
